import Application from "wms-core/Application";
import {Type} from "wms-core/Utils";
import Migration from "wms-core/db/Migration";
import CreateMigrationsTable from "wms-core/migrations/CreateMigrationsTable";
import CreateLogsTable from "wms-core/migrations/CreateLogsTable";
import ExpressAppComponent from "wms-core/components/ExpressAppComponent";
import NunjucksComponent from "wms-core/components/NunjucksComponent";
import MysqlComponent from "wms-core/components/MysqlComponent";
import LogRequestsComponent from "wms-core/components/LogRequestsComponent";
import RedisComponent from "wms-core/components/RedisComponent";
import ServeStaticDirectoryComponent from "wms-core/components/ServeStaticDirectoryComponent";
import MaintenanceComponent from "wms-core/components/MaintenanceComponent";
import MailComponent from "wms-core/components/MailComponent";
import SessionComponent from "wms-core/components/SessionComponent";
import RedirectBackComponent from "wms-core/components/RedirectBackComponent";
import FormHelperComponent from "wms-core/components/FormHelperComponent";
import CsrfProtectionComponent from "wms-core/components/CsrfProtectionComponent";
import WebSocketServerComponent from "wms-core/components/WebSocketServerComponent";
import HomeController from "./controllers/HomeController";
import AutoUpdateComponent from "wms-core/components/AutoUpdateComponent";

export default class App extends Application {
    private readonly port: number;

    constructor(port: number) {
        super(require('../package.json').version);
        this.port = port;
    }

    protected getMigrations(): Type<Migration>[] {
        return [
            CreateMigrationsTable,
            CreateLogsTable,
        ];
    }

    protected async init(): Promise<void> {
        this.registerComponents();
        this.registerWebSocketListeners();
        this.registerControllers();
    }

    private registerComponents() {
        const redisComponent = new RedisComponent();
        const mysqlComponent = new MysqlComponent();

        const expressAppComponent = new ExpressAppComponent(this.port);
        this.use(expressAppComponent);
        this.use(new NunjucksComponent());
        this.use(new LogRequestsComponent());

        // Static files
        this.use(new ServeStaticDirectoryComponent('public'));
        this.use(new ServeStaticDirectoryComponent('node_modules/feather-icons/dist', '/icons'));

        // Maintenance
        this.use(new MaintenanceComponent(this, () => {
            return redisComponent.canServe() && mysqlComponent.canServe();
        }));
        this.use(new AutoUpdateComponent());

        // Services
        this.use(mysqlComponent);
        this.use(new MailComponent());

        // Session
        this.use(redisComponent);
        this.use(new SessionComponent(redisComponent));

        // Utils
        this.use(new RedirectBackComponent());
        this.use(new FormHelperComponent());

        // Middlewares
        this.use(new CsrfProtectionComponent());

        // WebSocket server
        this.use(new WebSocketServerComponent(this, expressAppComponent, redisComponent));
    }

    private registerWebSocketListeners() {
    }

    private registerControllers() {
        this.use(new HomeController());
    }
}