import Controller from "wms-core/Controller";
import {Request, Response} from "express";

export default class HomeController extends Controller {
    routes(): void {
        this.get('/', this.getHome, 'home');
        this.get('/about', this.getAbout, 'about');
    }

    private async getHome(req: Request, res: Response) {
        res.render('home');
    }

    private async getAbout(req: Request, res: Response) {
        res.render('about');
    }
}