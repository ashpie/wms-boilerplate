import feather from "feather-icons";

document.addEventListener('DOMContentLoaded', () => {
    document.querySelectorAll('a[target="_blank"]').forEach(el => {
        el.innerHTML += `<i data-feather="external-link"></i>`;
    });

    feather.replace();
});