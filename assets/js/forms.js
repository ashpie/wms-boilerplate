// For labels to update their state (css selectors based on the value attribute)
document.addEventListener('DOMContentLoaded', () => {
    document.querySelectorAll('input').forEach(el => {
        if (el.type !== 'checkbox') {
            el.setAttribute('value', el.value);
            el.addEventListener('change', () => {
                el.setAttribute('value', el.value);
            });
        }
    });
});

window.applyFormMessages = function (formElement, messages) {
    for (const fieldName of Object.keys(messages)) {
        const field = formElement.querySelector('#field-' + fieldName);
        let parent = field.parentElement;
        while (parent && !parent.classList.contains('form-field')) parent = parent.parentElement;

        if (field) {
            let err = field.querySelector('.error');
            if (!err) {
                err = document.createElement('div');
                err.classList.add('error');
                parent.insertBefore(err, parent.querySelector('.hint') || parent);
            }
            err.innerHTML = `<i data-feather="x-circle"></i> ${messages[fieldName].message}`;
        }
    }
}