document.addEventListener('DOMContentLoaded', () => {
    document.querySelectorAll('.copyable-text').forEach(el => {
        const contentEl = el.querySelector('.content');
        contentEl.addEventListener('click', () => {
            window.getSelection().selectAllChildren(contentEl);
        });
        el.querySelector('.copy-button').addEventListener('click', () => {
            window.getSelection().selectAllChildren(contentEl);
            document.execCommand('copy');
        });
    });
});