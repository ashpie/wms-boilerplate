export default Object.assign(require("wms-core/config/default").default, {
    app: {
        name: 'Example App',
        contact_email: 'contact@example.net'
    },
    log_level: "DEV",
    db_log_level: "ERROR",
    public_url: "http://localhost:4899",
    public_websocket_url: "ws://localhost:4899",
    port: 4899,
    gitlab_webhook_token: 'secret',
    mysql: {
        connectionLimit: 10,
        host: "localhost",
        user: "root",
        password: "",
        database: "example_app",
        create_database_automatically: false
    },
    redis: {
        host: "127.0.0.1",
        port: 6379,
        prefix: 'example_app'
    },
    session: {
        secret: "very_secret_not_known",
        cookie: {
            secure: false
        }
    },
    mail: {
        host: "127.0.0.1",
        port: "1025",
        secure: false,
        username: "",
        password: "",
        allow_invalid_tls: true,
        from: 'contact@example.net',
        from_name: 'Example App',
    },
    view: {
        cache: false
    },
    approval_mode: false,
});