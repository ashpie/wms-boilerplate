export default Object.assign(require("wms-core/config/test").default, {
    mysql: {
        host: "localhost",
        user: "root",
        password: "",
        database: "wms2_test",
        create_database_automatically: true
    }
});