export default Object.assign(require("wms-core/config/production").default, {
    log_level: "DEBUG",
    db_log_level: "ERROR",
    public_url: "https://watch-my.stream",
    public_websocket_url: "wss://watch-my.stream",
    session: {
        cookie: {
            secure: true
        }
    },
    mail: {
        secure: true,
        allow_invalid_tls: false
    }
});