const path = require('path');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const UglifyJSPlugin = require('uglifyjs-webpack-plugin');

const dev = process.env.NODE_ENV === 'development';

const userConfig = require('./assets/config.json');
for (const b in userConfig.bundles) {
    if (userConfig.bundles.hasOwnProperty(b)) {
        userConfig.bundles[b] = `./assets/${userConfig.bundles[b]}`;
    }
}

const config = {
    entry: userConfig.bundles,
    output: {
        path: path.resolve(__dirname, 'public/js'),
        filename: '[name].js'
    },
    devtool: dev ? 'eval-source-map' : undefined,
    module: {
        rules: [
            {
                test: /\.js$/i,
                use: [
                    {
                        loader: 'babel-loader',
                        options: {
                            presets: ['@babel/preset-env'],
                        }
                    }
                ]
            },
            {
                test: /\.s[ac]ss$/i,
                use: [
                    {
                        loader: MiniCssExtractPlugin.loader,
                        options: {
                            publicPath: '/',
                        }
                    },
                    'css-loader',
                    'sass-loader',
                ]
            },
            {
                test: /\.(woff2?|eot|ttf|otf)$/i,
                use: 'file-loader?name=../fonts/[name].[ext]',
            },
            {
                test: /\.(png|jpe?g|gif|svg)$/i,
                use: [
                    'file-loader?name=../img/[name].[ext]',
                    {
                        loader: 'img-loader',
                        options: {
                            enabled: !dev,
                            plugins: [
                                require('imagemin-gifsicle')({}),
                                require('imagemin-mozjpeg')({}),
                                require('imagemin-pngquant')({}),
                                require('imagemin-svgo')({}),
                            ]
                        }
                    }
                ]
            }
        ],
    },
    plugins: [
        new MiniCssExtractPlugin({
            filename: '../css/[name].css',
        }),
    ]
};

if (!dev) {
    config.optimization = {
        minimizer: [
            new UglifyJSPlugin(),
        ]
    };
}

module.exports = config;